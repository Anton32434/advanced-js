class Employee {
    constructor(options) {
        this.name = options.name;
        this.age = options.age;
        this.salary = options.salary;
    }
    
    get nameToLowerCase() {
        const nameLower = this.name.toLowerCase();
        return nameLower;
    }

    set nameToLowerCase(value) {
        document.body.style.background = "red";

        this.name = value.toLowerCase();
    }


    get dateOfBirth() {
        const todayDate = new Date();
        const DateOfBirth = todayDate.getFullYear() - this.age;

        return DateOfBirth;
    }

    set dateOfBirth(value) {
        document.body.style.background = "green";
        this.age = value;

        console.log(`date of birth - ${this.dateOfBirth}`);
    }


    get salaryBonus() {
        const salaryWithBonus = this.salary + 2000;

        return salaryWithBonus;
    }

    set salaryBonus(value) {
        document.body.style.background = "yellow";
        this.salary = this.salaryBonus + this.salary;
    }
}

class Programmer extends Employee {
    constructor(options) {
        super(options);

        this.lang = options.lang;
    }
}

const employee = new Employee({
    name: "Anton",
    age: 88, 
    salary: 2000,
});

const programmer1 = new Programmer({
    name: "John",
    age: 20,
    salary: 40000,
    lang: 3,
});

const programmer2 = new Programmer({
    name: "Dan",
    age: 50,
    salary: 70000,
    lang: 1,
});


console.log(employee);

console.log(programmer1);
console.log(programmer2)