const root = document.querySelector("#root");


const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
  },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
  },
    {
        name: "Тысячекратная мысль",
        price: 70
  },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
  },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40,
        age: 12323
  },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
  }
];




books.forEach(item => {
    try {
        if (
            item.hasOwnProperty("author") &&
            item.hasOwnProperty("name") &&
            item.hasOwnProperty("price")
        ) {
            const ul = document.createElement("ul");
            root.appendChild(ul);
            
            
            const objKeys = Object.keys(item);
            const objValues = Object.values(item);
            const objKeysLength = objKeys.length;
            
            for (let i = 0; i < objKeysLength; i++) {
                const li = document.createElement("li");
                
                li.innerHTML = `${objKeys[i]} - ${objValues[i]}`;
                ul.appendChild(li);
            }
        }
        
        
        const checkObjKeys = ["author", "name", "price"];
        
        for (let key of checkObjKeys) {
            if (item[key] === undefined) throw new Error(key);
        }
        
    } catch (err) {
        console.log(`Object has no key - ${err.message}`)
    }
})
